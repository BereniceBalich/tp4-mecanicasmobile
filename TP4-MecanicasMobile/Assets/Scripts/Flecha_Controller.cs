using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flecha_Controller : MonoBehaviour
{

    //public Mecanica13_Controller m13;
    public bool estadoDeJuego;
    public Rigidbody rbFlecha;
    public GameObject objFlechaPrefab;
    public Transform transformFlechaPrefab;


    public void start()
    {
        //GameObject otroObjeto2 = GameObject.Find("Mecanica13_Controller");
        //m13 = otroObjeto2.GetComponent<Mecanica13_Controller>();
        rbFlecha = GetComponent<Rigidbody>();
        transformFlechaPrefab = objFlechaPrefab.GetComponent<Transform>();
    }
    public void Update()
    {
        //Debug.Log(m13.ComparaEstado + "comprobado");
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "flechaDisparada")
        {
            // Debug.Log("Flecha choco con flecha");

            estadoDeJuego = false;

        }
    }




}
