using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparador_Controller : MonoBehaviour
{
    public GameObject blancoController;
    public GameObject[] flechasDisparadas;
    public GameObject disparador;
    public GameObject flechaPrefab;
    public GameObject blancoObjetivoObj;
    public Transform transformBlancoObjetivo;
    public float projectileSpeed = 120f;
    public GameObject Disparador;


    public bool noSePuedeDisparar = false;
    public void start()
    {

        transformBlancoObjetivo = blancoObjetivoObj.GetComponent<Transform>();
    }
    void Update()
    {

        flechasDisparadas = GameObject.FindGameObjectsWithTag("flechaDisparada");

        foreach (GameObject objeto in flechasDisparadas)
        {

            Flecha_Controller f = objeto.GetComponent<Flecha_Controller>();
            if (f != null && f.estadoDeJuego == false)
            {
                EliminarFlechas();
                Debug.Log("Flechas " + f.estadoDeJuego);
                noSePuedeDisparar = true;
                Blanco_Controller b = blancoController.GetComponent<Blanco_Controller>();
                b.rotar = false;

            }
        }
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (IsTouchOverObject(touch.position))
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began && noSePuedeDisparar == false)
                {

                    Vector3 direction = (transformBlancoObjetivo.position - transform.position).normalized;
                    GameObject flecha = Instantiate(flechaPrefab, disparador.transform.position, flechaPrefab.transform.rotation);
                    flecha.name = "flechaDisparada";
                    flecha.tag = "flechaDisparada";
                    flecha.GetComponent<Rigidbody>().velocity = direction * projectileSpeed;
                }
            }
        }
    }
    public void EliminarFlechas()
    {
        GameObject[] flechas = GameObject.FindGameObjectsWithTag("flechaDisparada");
        for (int i = 0; i < flechas.Length; i++)
        {
            Destroy(flechas[i]);

        }
    }
    private bool IsTouchOverObject(Vector2 touchPosition)
    {
        Ray ray = Camera.main.ScreenPointToRay(touchPosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform == transform)
            {
                // El toque está sobre este objeto
                return true;
            }
        }

        return false;
    }

}
