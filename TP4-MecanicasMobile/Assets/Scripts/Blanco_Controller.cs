using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blanco_Controller : MonoBehaviour
{
    public bool rotar = true;
    public Rigidbody rbFlecha;
    public float rotationSpeed = 80f; // Velocidad de rotación en grados por segundo
    public GameObject blancoObjetivoObj;
    public Transform transformBlancoObjetivo;
    void start()
    {
        rotar = true;
        transformBlancoObjetivo = blancoObjetivoObj.GetComponent<Transform>();
    }
    void Update()
    {
        if (rotar == true)
        {
            // Obtener la rotación actual del objeto
            Quaternion currentRotation = transformBlancoObjetivo.rotation;

            // Calcular la rotación adicional en el eje Z
            Quaternion zRotation = Quaternion.Euler(0f, 0f, rotationSpeed * Time.deltaTime);

            // Aplicar la rotación adicional al objeto
            transformBlancoObjetivo.rotation = currentRotation * zRotation;

        }

    }
    public void OnCollisionEnter(Collision collision)
    {


        collision.transform.SetParent(blancoObjetivoObj.transform);
        collision.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;

    }
}
