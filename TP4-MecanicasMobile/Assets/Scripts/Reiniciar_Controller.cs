using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Reiniciar_Controller : MonoBehaviour
{
    public GameObject d;
    public GameObject b;
     Disparador_Controller disparador;
     Blanco_Controller blancoObjetivo;

    public void start()
    {
        d = GameObject.Find("Disparador");
        b = GameObject.Find("BlancoObjetivo");
       
    }


    public void Update()
    {
        if (Input.touchCount > 0)
        {
           
            Touch touch = Input.GetTouch(0);
            if (IsTouchOverObject(touch.position))
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {   
                    Blanco_Controller blancoObjetivo = b.GetComponent<Blanco_Controller>();
                    Disparador_Controller disparador = d.GetComponent<Disparador_Controller>();
                    disparador.noSePuedeDisparar = false;
                    blancoObjetivo.rotar = true;
                    Debug.Log("boton presionado");
                }
            }
        }
    }
    private bool IsTouchOverObject(Vector2 touchPosition)
    {
        Ray ray = Camera.main.ScreenPointToRay(touchPosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform == transform)
            {
                // El toque está sobre este objeto
                return true;
            }
        }

        return false;
    }

}
